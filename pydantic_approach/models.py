import datetime

from pydantic import BaseModel, field_serializer
from typing_extensions import Annotated

import pydantic_approach.utils as utils


class Employee(BaseModel):
    id: int
    name: str


class Task(BaseModel):
    id: int
    name: str


class Availability(BaseModel):
    employee: Employee
    start_datetime: datetime.datetime
    end_datetime: datetime.datetime

    @field_serializer("employee")
    def serialize_employee(self, employee: Employee):
        return employee.id


class Qualification(BaseModel):
    employee: Employee
    task: Task
    start_datetime: datetime.datetime
    end_datetime: datetime.datetime

    @field_serializer("employee")
    def serialize_employee(self, employee: Employee):
        return employee.id

    @field_serializer("task")
    def serialize_task(self, task: Task):
        return task.id


class FixedShift(BaseModel):
    employee: Employee
    tasks: list[Task]
    start_datetime: datetime.datetime
    end_datetime: datetime.datetime

    @field_serializer("employee")
    def serialize_employee(self, employee: Employee):
        return employee.id

    @field_serializer("tasks")
    def serialize_tasks(self, tasks: list[Task]):
        return [task.id for task in tasks]


RelatedAvailability = Annotated[
    Availability,
    utils.RelatedObjectValidator("employee", "id", "employees"),
]
RelatedQualification = Annotated[
    Qualification,
    utils.RelatedObjectValidator("task", "id", "tasks"),
    utils.RelatedObjectValidator("employee", "id", "employees"),
]
RelatedFixedShift = Annotated[
    FixedShift,
    utils.RelatedObjectValidator("employee", "id", "employees"),
    utils.RelatedObjectValidator("tasks", "id", "tasks", many=True),
]


class EngineInput(BaseModel):
    employees: list[Employee]
    tasks: list[Task]
    availabilities: list[RelatedAvailability]
    qualifications: list[RelatedQualification]
    fixed_shifts: list[RelatedFixedShift]
