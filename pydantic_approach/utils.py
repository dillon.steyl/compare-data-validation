from typing import Any, Optional

from pydantic import (
    BaseModel,
    FieldValidationInfo,
    GetCoreSchemaHandler,
)
from pydantic_core import CoreSchema, core_schema


class LookupContext:
    def __init__(self):
        """
        Creates a context object that can be passed to `model_validate_json` to store lookups
        by name.
        """
        self.lookups_by_name: dict[str, dict[Any, Any]] = {}

    def get_or_create_lookup_dict(
        self, lookup_name: str, objects: list[BaseModel], identifier_field_name: str
    ) -> dict[Any, Any]:
        """
        Gets or creates a lookup dictionary for the given lookup name and objects.
        Uses the identifier_field_name to determine the key for the lookup dictionary.
        """
        if lookup_name not in self.lookups_by_name:
            self.lookups_by_name[lookup_name] = {
                getattr(obj, identifier_field_name): obj for obj in objects
            }
        return self.lookups_by_name[lookup_name]


class RelatedObjectValidator:
    def __init__(
        self,
        related_field_name: str,
        identifier_field_name: str,
        related_objects_field_name: str,
        many: bool = False,
    ) -> None:
        """
        Creates a validator that can be used to handle foreign key lookups.

        Args:
            related_field_name (str): The name of the field that contains the identifier of the related object.
            identifier_field_name (str): The name of the identifying field for the related object.
            related_objects_field_name (str): The name of the field that contains all of
                the related objects.

        Example:
            RelatedObjectValidator("employee", "id", "employees") will create a validator that
                builds an "employee_by_id" lookup based on the "employees" attribute. It will then
                apply that lookup to the "employee" field on whichever object is being validated.

        Usage:
            class Employee(BaseModel):
                id: int
                name: str

            class Availability(BaseModel):
                employee: Employee
                start_datetime: str
                end_datetime: str

            RelatedAvailability = Annotated[Availability, RelatedObjectValidator("employee", "id", "employees")]

            class EngineInput(BaseModel):
                employees: list[Employee]
                availabilities: list[RelatedAvailability]
        """
        self.related_field_name = related_field_name
        self.identifier_field_name = identifier_field_name
        self.related_objects_field_name = related_objects_field_name
        self.many = many

    @property
    def lookup_name(self) -> str:
        """
        Gets the name of the lookup that will be constructed and stored on the validation context.
        e.g. tasks_by_id or employees_by_id
        """
        return f"{self.related_objects_field_name}_by_{self.identifier_field_name}"

    def _make_lookup_handler(self) -> core_schema.WithInfoValidatorFunction:
        """
        Creates and returns a lookup handler function which is called before validation.
        This function is responsible for applying the appropriate foreign key lookup to the
        `related_field_name`, e.g. to replace an ID with an entire object.
        """

        def lookup_handler(value: dict[str, Any], info: FieldValidationInfo):
            if not isinstance(info.context, LookupContext):
                raise ValueError(
                    "Context of validation must be a LookupContext. "
                    "e.g. MyModel.model_validate(data, context=LookupContext())"
                )

            lookup_dict = info.context.get_or_create_lookup_dict(
                lookup_name=self.lookup_name,
                objects=info.data.get(self.related_objects_field_name),
                identifier_field_name=self.identifier_field_name,
            )
            if self.many:
                related_objects = [
                    lookup_dict.get(item) for item in value[self.related_field_name]
                ]
                if not all(related_objects):
                    raise ValueError(
                        f"Could not find related object(s) {related_objects} in {self.related_objects_field_name}"
                    )
                value[self.related_field_name] = related_objects
            else:
                related_object = lookup_dict.get(value[self.related_field_name])
                if not related_object:
                    raise ValueError(
                        f"Could not find related object {value[self.related_field_name]} in {self.related_objects_field_name}"
                    )
                value[self.related_field_name] = related_object

            return value

        return lookup_handler

    def __get_pydantic_core_schema__(
        self, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.with_info_before_validator_function(
            function=self._make_lookup_handler(),
            field_name=source_type.__name__.lower(),
            schema=handler(source_type),
        )
