import pydantic_approach.models as pydantic_models
import pydantic_approach.utils as pydantic_utils
import marshmallow_approach.schemas as marshmallow_schemas
import json
import time

INPUT_FP = "engine_input.json"


def read_input_pydantic():
    time_start = time.time()
    with open(INPUT_FP, "r") as f:
        engine_input = pydantic_models.EngineInput.model_validate_json(
            f.read(), context=pydantic_utils.LookupContext()
        )
    print(f"Pydantic: {time.time() - time_start}")
    return engine_input


def read_input_marshmallow():
    time_start = time.time()
    with open(INPUT_FP, "r") as f:
        json_data = json.load(f)
        engine_input = (
            marshmallow_schemas.EngineInputSchema(strict=True, context={})
            .load(json_data)
            .data
        )
    print(f"Marshmallow: {time.time() - time_start}")
    return engine_input


marshmallow_input = read_input_marshmallow()
pydantic_input = read_input_pydantic()
