## Comparing Data Validation Libraries

A very basic repo to compare the performance of data validation approaches (e.g. marshmallow vs pydantic)
The `read_input.py` file applies both approaches to the included `engine_input.json` file.
