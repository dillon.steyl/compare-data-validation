import factories as factories
import pydantic_approach.models as models
import random
import json


NUM_TASKS = 100
NUM_EMPLOYEES = 2000
NUM_QUALIFICATIONS = 1200
NUM_AVAILABILITIES = 2000
NUM_FIXED_SHIFTS = 7000


if __name__ == "__main__":
    employees = factories.EmployeeFactory.create_batch(NUM_EMPLOYEES)
    tasks = factories.TaskFactory.create_batch(NUM_TASKS)

    availabilities = [
        factories.AvailabilityFactory.create(employee=random.choice(employees))
        for _ in range(NUM_AVAILABILITIES)
    ]
    qualifications = [
        factories.QualificationFactory.create(
            employee=random.choice(employees), task=random.choice(tasks)
        )
        for _ in range(NUM_QUALIFICATIONS)
    ]
    fixed_shifts = [
        factories.FixedShiftFactory.create(
            employee=random.choice(employees),
            tasks=[random.choice(tasks) for _ in range(3)],
        )
        for _ in range(NUM_FIXED_SHIFTS)
    ]

    engine_input = models.EngineInput.model_construct(
        tasks=tasks,
        employees=employees,
        availabilities=availabilities,
        qualifications=qualifications,
        fixed_shifts=fixed_shifts,
    )
    json_data = engine_input.model_dump_json(indent=2)
    with open("engine_input.json", "w") as f:
        f.write(json_data)
