import datetime
from dataclasses import dataclass


@dataclass(frozen=True)
class Employee:
    id: int
    name: str


@dataclass(frozen=True)
class Task:
    id: int
    name: str


@dataclass(frozen=True)
class Availability:
    employee: Employee
    start_datetime: datetime.datetime
    end_datetime: datetime.datetime


@dataclass(frozen=True)
class Qualification:
    employee: Employee
    task: Task
    start_datetime: datetime.datetime
    end_datetime: datetime.datetime


@dataclass(frozen=True)
class FixedShift:
    employee: Employee
    tasks: list[Task]
    start_datetime: datetime.datetime
    end_datetime: datetime.datetime


@dataclass(frozen=True)
class EngineInput:
    employees: list[Employee]
    tasks: list[Task]
    availabilities: list[Availability]
    qualifications: list[Qualification]
    fixed_shifts: list[FixedShift]
