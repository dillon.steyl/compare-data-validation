from operator import attrgetter
import dataclasses
from bkt_marshmallow import BaseSchema, Relationship
from bkt_marshmallow.fields import dictstore
from marshmallow import fields

import marshmallow_approach.models as models


class EmployeeSchema(BaseSchema):
    __model__ = models.Employee

    id = fields.Integer(required=True)
    name = fields.String(required=True)


class TaskSchema(BaseSchema):
    __model__ = models.Task

    id = fields.Integer(required=True)
    name = fields.String(required=True)


class AvailabilitySchema(BaseSchema):
    __model__ = models.Availability

    employee = Relationship("id", "employees", many=False, required=True)
    start_datetime = fields.DateTime(required=True)
    end_datetime = fields.DateTime(required=True)


class QualificationSchema(BaseSchema):
    __model__ = models.Qualification

    employee = Relationship("id", "employees", many=False, required=True)
    task = Relationship("id", "tasks", many=False, required=True)
    start_datetime = fields.DateTime(required=True)
    end_datetime = fields.DateTime(required=True)


class FixedShiftSchema(BaseSchema):
    __model__ = models.FixedShift

    employee = Relationship("id", "employees", many=False, required=True)
    tasks = Relationship("id", "tasks", many=True, required=True)
    start_datetime = fields.DateTime(required=True)
    end_datetime = fields.DateTime(required=True)


class EngineInputSchema(BaseSchema):
    __model__ = models.EngineInput

    tasks = dictstore(
        attrgetter("id"), fields.Nested, TaskSchema, many=True, required=True
    )
    employees = dictstore(
        attrgetter("id"), fields.Nested, EmployeeSchema, many=True, required=True
    )
    availabilities = fields.Nested(AvailabilitySchema, many=True, required=True)
    qualifications = fields.Nested(QualificationSchema, many=True, required=True)
    fixed_shifts = fields.Nested(FixedShiftSchema, many=True, required=True)
