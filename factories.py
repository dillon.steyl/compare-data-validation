import factory
from factory import fuzzy

import datetime
import pydantic_approach.models as models
import pytz


class EmployeeFactory(factory.Factory):
    class Meta:
        model = models.Employee

    id = factory.Sequence(lambda x: x)
    name = factory.Faker("name")


class TaskFactory(factory.Factory):
    class Meta:
        model = models.Task

    id = factory.Sequence(lambda x: x)
    name = factory.Faker("name")


class AvailabilityFactory(factory.Factory):
    class Meta:
        model = models.Availability

    employee = factory.SubFactory(EmployeeFactory)
    start_datetime = fuzzy.FuzzyDateTime(
        start_dt=datetime.datetime.now(pytz.UTC),
        end_dt=datetime.datetime.now(pytz.UTC) + datetime.timedelta(days=10),
    )
    end_datetime = fuzzy.FuzzyDateTime(
        start_dt=datetime.datetime.now(pytz.UTC),
        end_dt=datetime.datetime.now(pytz.UTC) + datetime.timedelta(days=10),
    )


class QualificationFactory(factory.Factory):
    class Meta:
        model = models.Qualification

    employee = factory.SubFactory(EmployeeFactory)
    task = factory.SubFactory(EmployeeFactory)
    start_datetime = fuzzy.FuzzyDateTime(
        start_dt=datetime.datetime.now(pytz.UTC),
        end_dt=datetime.datetime.now(pytz.UTC) + datetime.timedelta(days=10),
    )
    end_datetime = fuzzy.FuzzyDateTime(
        start_dt=datetime.datetime.now(pytz.UTC) + datetime.timedelta(days=100),
        end_dt=datetime.datetime.now(pytz.UTC) + datetime.timedelta(days=365),
    )


class FixedShiftFactory(factory.Factory):
    class Meta:
        model = models.FixedShift

    employee = factory.SubFactory(EmployeeFactory)
    tasks = factory.List([factory.SubFactory(TaskFactory) for _ in range(3)])
    start_datetime = fuzzy.FuzzyDateTime(
        start_dt=datetime.datetime.now(pytz.UTC),
        end_dt=datetime.datetime.now(pytz.UTC) + datetime.timedelta(days=10),
    )
    end_datetime = factory.LazyAttribute(
        lambda o: o.start_datetime + datetime.timedelta(hours=8)
    )
